import com.devcamp.jbr1s20.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
       
        Rectangle rectangle1  = new Rectangle();
        Rectangle rectangle2  = new Rectangle(3.0f, 2.0f);

        System.out.println(rectangle1);
        System.out.println(rectangle2);
    }
}
